<?php

namespace restapi\validators;

use common\models\UsersTokens;
use restapi\controllers\AppController;
use yii;

class AuthTokenValidator extends \yii\validators\Validator
{
    /**
     * @var AppController
     */
    public $controller;

    protected function validateValue($value)
    {
        $userToken = UsersTokens::getTokenWithUserByToken(Yii::$app->request->get('auth_token'));
        $this->controller->_user = $userToken->user;
//        $this->controller->_token = $userToken;
    }
}