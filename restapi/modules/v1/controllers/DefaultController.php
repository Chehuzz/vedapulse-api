<?php

namespace restapi\modules\v1\controllers;

use restapi\controllers\AppController;
use restapi\filters\RequestInputFilter;
use common\models\UserData;

class DefaultController extends AppController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['input-params'] = [
            'class' => RequestInputFilter::className(),
            'rules' => [
                'average' => [
                    [['user_id'], 'required', 'on' => 'get'],
                ]
            ]
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        return $this->render['index'];
    }

    public function actionAverage()
    {
        $user_id = $this->input['user_id'];
        $data = UserData::find()->where(['user_id' => $user_id])->one();
        $data = json_decode($data->data, true);

        $sum = 0;

        for($i = 0; $i < count($data); $i++){
            $sum += $data[$i];
        }

        return ['average' => $sum/count($data)];
    }
}
