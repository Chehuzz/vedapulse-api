<?php
namespace restapi\controllers;

use Yii;
use yii\rest\Controller;

/**
 * App controller
 */
class AppController extends Controller
{
    /**
     * @var array
     */
    protected $input;

    public function init()
    {
        $this->input = Yii::$app->request->isGet ? Yii::$app->request->get() : Yii::$app->request->bodyParams;
        parent::init();
    }
}
