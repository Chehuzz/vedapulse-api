<?php

use yii\db\Migration;

class m160315_111943_user_data_table extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE user_data (
         id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
         user_id int(11) UNSIGNED NOT NULL,
         data TEXT DEFAULT NULL,
         PRIMARY KEY (id)
        )ENGINE = INNODB
        CHARACTER SET utf8
        COLLATE utf8_general_ci;

        ALTER TABLE user_data
        ADD CONSTRAINT FK_user_data_ref_user FOREIGN KEY (user_id)
        REFERENCES user (id) ON DELETE CASCADE ON UPDATE CASCADE
        ;");
    }

    public function down()
    {
        echo "m160315_111943_user_data_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
